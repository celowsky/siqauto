#!/usr/bin/env node
const puppeteer = require('puppeteer');
const shellExec = require('shell-exec');
const readline = require('readline');
const fs = require('fs');
const argv = require('minimist')(process.argv.slice(2));

function randomString(length, chars) {
    let result = '';
    for (let i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}

// User configurable variables
const emailPrefix = argv['u'] ? argv['u'] : randomString(32, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
const emailDomain = argv['d'] ? argv['d'] : 'mailinator.com';
const password = argv['p'] ? argv['p'] : 'asdfASDF1234!@#$';
const headless = !!argv['headless'];
const environment = argv['e'] ? argv['e'] : 'dev';
let siteUrl;
if (environment === 'dev') {
    siteUrl = 'https://siq-local.infosecinstitute.com';
} else if (environment === 'stage') {
    siteUrl = 'https://phishstaging.infosecinstitute.com';
} else if (environment === 'prod') {
    siteUrl = 'https://securityiq.infosecinstitute.com';
}
const myTimeout = 60000;

// Get info about user's env
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});
let config;
if (fs.existsSync('.config')) {
    config = JSON.parse(fs.readFileSync('.config'));
} else {
    config = {};
}
if (!config.hasOwnProperty('projectDir')) {
    rl.question('Enter the absolute path of your SIQ project directory: ', answer => {
        if (!answer) {
            throw new Error('Please provide a project directory to use');
        }
        config.projectDir = answer;
        fs.writeFileSync('.config', JSON.stringify(config));

        makeUser(environment, emailPrefix, emailDomain, password, config.projectDir)
            .then(() => {
                console.log(`User email is ${emailPrefix}@${emailDomain}`);
                console.log(`User password is ${password}`);
                rl.close();
            });
    });

}

async function makeUser(environment, emailPrefix, emailDomain, password, projectDir) {
    console.log('Started making user');
    const email = `${emailPrefix}@${emailDomain}`;
    const browser = await puppeteer.launch({
        headless: headless,
    });
    const page = await browser.newPage();
    const userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0';
    await page.setUserAgent(userAgent);
    await page.goto(`${siteUrl}/users/add`);

    // Fill out registration form
    await page.waitFor('input#email');
    await page.$eval('input#email', (el, email) => el.value = email, email);
    await page.$eval('input#password', (el, password) => el.value = password, password);
    await page.$eval('input#confirm-password', (el, password) => el.value = password, password);
    await page.$eval('input#name', el => el.value = 'TestUser');
    await page.$eval('input#first-name', el => el.value = 'Test');
    await page.$eval('input#last-name', el => el.value = 'User');
    await page.$eval('input#work-phone', el => el.value = '1234567890');
    await page.$eval('select[name="company_size"]', el => el.value = 0);
    await page.$eval('select[name="buying_stage"]', el => el.value = 0);
    await page.click('input#tos');
    await page.click('#registerForm > div.block-content.template-editor.pad-10 > button');
    // TODO: See if form submitted


    // Navigate to email window
    if (environment !== 'dev') {
        await page.setUserAgent(userAgent); // TODO: Check if this is necessary
        await page.goto(`https://www.mailinator.com`);
        await page.waitFor('input#inboxfield');
        await page.$eval('input#inboxfield', (el, email) => el.value = email, email);
        await page.click('.btn');

        const selector = 'tbody > tr td:nth-child(4)';
        const waitForEmail = await page.waitFor((selector) => {
            let element = document.querySelector(selector);
            if (element && element.innerText && element.innerText.indexOf('SecurityIQ') !== -1) {
                return element;
            } else {
                return false;
            }
        }, {timeout: myTimeout}, selector);

        // Click on link in SIQ Registration email
        await waitForEmail.click();

        // Wait for the email to load, then click the link
        await page.waitFor(2000);
        const siqRegistrationLinkiFrame = await page.frames().find(f => f.name() === 'msg_body');
        const siqLink = await siqRegistrationLinkiFrame.evaluate(() => {
            return document.body.querySelector('#templateBody > tbody > tr > td > div > ol > li:nth-child(1) > a').getAttribute('href');
        });
        await page.goto(siqLink);
    } else {
        // Run bin/cake queue
        const shellResult = await shellExec(`cd ${projectDir} && vagrant ssh -- -t \'cd /var/www && ./bin/cake queue\'\n`);
        console.log(`shellResult is ${JSON.stringify(shellResult)}`);
        await page.waitFor(2000);
        await page.goto(`http://siq-local.infosecinstitute.com:1080`);
        const selector = '#messages > table > tbody > tr:nth-child(1) > td:nth-child(3)';
        const firstRow = await page.waitFor((selector) => {
            let element = document.querySelector(selector);
            if (element && element.innerText.indexOf('SecurityIQ Registration Verification') !== -1) {
                return element;
            } else {
                return false;
            }
        }, {}, selector);
        await firstRow.click();
        await page.click('#message > header > nav > ul > li.format.tab.plain:nth-child(2) > a');
        await page.click('#message > header > nav > ul > li.format.tab.html:nth-child(1) > a');
        await page.waitFor(1000);

        // Get email message iframe
        const frameElement = page.frames().find(f => f._navigationURL.indexOf('messages') !== -1);
        const registrationLinkElement = await frameElement.$('#templateBody > tbody > tr > td > div > ol > li:nth-child(1) > a');
        const link = await frameElement.evaluate(link => link.href, registrationLinkElement);
        console.log(`link is ${link}`);
        await page.goto(link);
    }

    // Navigate to SIQ additional info page
    await page.waitFor('input#mobile-phone');
    await page.$eval('input#job-title', el => el.value = 'Test');
    await page.$eval('input#mobile-phone', el => el.value = '1234567890');
    await page.$eval('select[name="industry"]', el => el.value = 0);
    await page.$eval('select[name="country"]', el => el.value = 0);
    await page.$eval('select[name="anticipated_learners"]', el => el.value = 0);
    await page.$eval('select[name="career_level"]', el => el.value = 0);
    await page.$eval('select[name="timeline"]', el => el.value = 0);
    await page.$eval('input#current-phish-provider', el => el.value = 'Test');
    await page.$eval('select[name="epp_vendor"]', el => el.value = 0);
    await page.$eval('select[name="timezone"]', el => el.value = 'Africa/Abidjan');
    await page.click('button#submit');

    await browser.close();
    return true;
}

